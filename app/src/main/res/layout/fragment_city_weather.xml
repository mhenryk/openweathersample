<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="vm"
            type="com.mhmasny.openweathersample.ui.screens.vm.CityWeatherViewModel"/>
    </data>

    <android.support.constraint.ConstraintLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="@dimen/std_margin">

        <TextView
            android:id="@+id/city_name"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@{vm.currentWeather.name}"
            app:layout_constraintEnd_toEndOf="parent"
            android:textStyle="bold"
            android:textSize="20sp"
            android:textColor="@android:color/black"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:text="Warszawa"/>

        <TextView
            android:id="@+id/sunrise_title"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/sunrise_title_text"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"/>

        <TextView
            android:id="@+id/sunrise_at"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/sunrise_title"
            app:time="@{vm.currentWeather.sys.sunrise}"/>

        <TextView
            android:id="@+id/sunset_title"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/sunset_title_text"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent"/>

        <TextView
            android:id="@+id/sunset_at"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toBottomOf="@id/sunset_title"
            app:time="@{vm.currentWeather.sys.sunset}"/>

        <TextView
            android:id="@+id/city_temperature"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:temperature="@{String.valueOf(vm.currentWeather.main.temp)}"
            app:layout_constraintEnd_toStartOf="@+id/humidity"
            app:layout_constraintHorizontal_chainStyle="packed"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/sunrise_at"
            tools:text="100"/>

        <TextView
            android:id="@+id/humidity"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:humidity="@{String.valueOf(vm.currentWeather.main.humidity)}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_chainStyle="packed"
            app:layout_constraintStart_toEndOf="@id/city_temperature"
            app:layout_constraintTop_toBottomOf="@id/sunrise_at"
            tools:text="50%"/>

        <ImageView
            android:id="@+id/icon"
            android:layout_width="80dp"
            android:layout_height="80dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/humidity"
            app:visibleGone="@{vm.isLoading}"
            app:weatherIcon="@{vm.currentWeather.weather}"/>

        <android.support.v4.widget.ContentLoadingProgressBar
            android:id="@+id/progressbar"
            style="?android:progressBarStyleSmall"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:indeterminateOnly="true"
            app:layout_constraintTop_toBottomOf="@id/humidity"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:visibleGone="@{!vm.isLoading}"/>

        <TextView
            android:id="@+id/weather_description_main"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toStartOf="@id/weather_description_sub"
            app:layout_constraintHorizontal_chainStyle="packed"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/icon"
            app:weatherMain="@{vm.currentWeather.weather}"
            tools:text="Cloudy"/>

        <TextView
            android:id="@+id/weather_description_sub"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_chainStyle="packed"
            app:layout_constraintStart_toEndOf="@id/weather_description_main"
            app:layout_constraintTop_toBottomOf="@id/icon"
            app:weatherSub="@{vm.currentWeather.weather}"
            tools:text="Cloudy"/>

        <ImageView
            android:id="@+id/wind_icon"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:src="@drawable/ic_arrow_upward_black_24dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/weather_description_sub"
            app:rotation="@{vm.currentWeather.wind.deg}"/>

        <TextView
            android:id="@+id/wind_speed"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:windSpeed="@{String.valueOf(vm.currentWeather.wind.speed)}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/wind_icon"/>

        <TextView
            android:id="@+id/date"
            style="@style/PaddedText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:date="@{vm.currentWeather.dt}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            tools:text="Tuesday"/>

    </android.support.constraint.ConstraintLayout>
</layout>
