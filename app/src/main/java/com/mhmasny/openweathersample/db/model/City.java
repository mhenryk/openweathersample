package com.mhmasny.openweathersample.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.mhmasny.openweathersample.model.js2p.CurrentWeather;

/**
 * Created by Mateusz on 2/10/2018.
 */

@SuppressWarnings("WeakerAccess")
@Entity
public class City {
    @PrimaryKey(autoGenerate = false)
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "coord")
    private Coord coord;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "currentWeather")
    private CurrentWeather currentWeather;

    public CurrentWeather getCurrentWeather() {
        return currentWeather;
    }

    public void setCurrentWeather(CurrentWeather currentWeather) {
        this.currentWeather = currentWeather;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coords) {
        this.coord = coords;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", coord=" + coord +
                ", country='" + country + '\'' +
                ", currentWeather=" + currentWeather +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (getId() != city.getId()) return false;
        if (getName() != null ? !getName().equals(city.getName()) : city.getName() != null) return false;
        if (getCoord() != null ? !getCoord().equals(city.getCoord()) : city.getCoord() != null) return false;
        if (getCountry() != null ? !getCountry().equals(city.getCountry()) : city.getCountry() != null) return false;
        return getCurrentWeather() != null ? getCurrentWeather().equals(city.getCurrentWeather())
                                           : city.getCurrentWeather() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getCoord() != null ? getCoord().hashCode() : 0);
        result = 31 * result + (getCountry() != null ? getCountry().hashCode() : 0);
        result = 31 * result + (getCurrentWeather() != null ? getCurrentWeather().hashCode() : 0);
        return result;
    }
}

