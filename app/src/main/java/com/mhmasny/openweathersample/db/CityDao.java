package com.mhmasny.openweathersample.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.mhmasny.openweathersample.db.model.City;

import java.util.List;

@Dao
public interface CityDao {
    @Query("SELECT * FROM city")
    List<City> getAll();

    @Query("SELECT * FROM city WHERE id IN (:cityIds)")
    List<City> loadAllByIds(int[] cityIds);

    @Query("SELECT * FROM city WHERE id IS :id")
    City readCity(long id);

    @Query("SELECT * FROM city WHERE name LIKE :name")
    List<City> queryByName(String name);

    @Query("SELECT COUNT(id) FROM city")
    int getNumberOfRows();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(City... users);

    @Delete
    void delete(City user);
}
