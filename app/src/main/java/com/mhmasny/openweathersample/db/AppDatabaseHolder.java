package com.mhmasny.openweathersample.db;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Mateusz on 2/10/2018.
 */

public class AppDatabaseHolder {
    private static AppDatabaseHolder instance;
    private AppDatabase mDatabase;

    public synchronized static void init(@NonNull Context ctx) {
        instance = new AppDatabaseHolder();
        instance.createDatabase(ctx);
    }

    public static AppDatabase getDatabase() {
        return instance.mDatabase;
    }

    private void createDatabase(Context ctx) {
        mDatabase = Room.databaseBuilder(ctx, AppDatabase.class, "database-name").build();
    }
}
