package com.mhmasny.openweathersample.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.mhmasny.openweathersample.model.js2p.CurrentWeather;

/**
 * Created by Mateusz on 2/11/2018.
 */

public class CurrentWeatherConverter {

    //I could technically break this down to entities but the class is generated and it's just for the sake of example

    @TypeConverter
    public static CurrentWeather fromJson(String value) {
        return new Gson().fromJson(value, CurrentWeather.class);

    }

    @TypeConverter
    public static String toJson(CurrentWeather currWeather) {
        return new Gson().toJson(currWeather);

    }
}
