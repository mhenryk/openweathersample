package com.mhmasny.openweathersample.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.mhmasny.openweathersample.db.model.City;

@TypeConverters({CoordConverter.class, CurrentWeatherConverter.class})
@Database(entities = {City.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CityDao cityDao();
}
