package com.mhmasny.openweathersample.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.mhmasny.openweathersample.db.model.Coord;

/**
 * Created by Mateusz on 2/10/2018.
 */

public class CoordConverter {

    @TypeConverter
    public static Coord fromJson(String value) {
        return new Gson().fromJson(value, Coord.class);

    }

    @TypeConverter
    public static String toJson(Coord coord) {
        return new Gson().toJson(coord);

    }

}
