package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;

/**
 * Created by Mateusz on 2/8/2018.
 */

public interface MainActivityViewModel {
    void selectCity(@NonNull City city);
    LiveData<City> selectedCity();
}
