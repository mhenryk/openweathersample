package com.mhmasny.openweathersample.ui.screens;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mhmasny.openweathersample.R;

public class SeparatorDecoration extends RecyclerView.ItemDecoration {

    private final Drawable mDivider;
    private float   mLeftPadding     = 0;
    private float   mRightPadding    = 0;

    public SeparatorDecoration(@NonNull Context context, @DrawableRes int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
        mLeftPadding = context.getResources().getDimension(R.dimen.small_margin);
        mRightPadding = mLeftPadding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = mDivider.getIntrinsicHeight();
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft() + (int) mLeftPadding;
        int right = parent.getWidth() - parent.getPaddingRight() - (int) mRightPadding;

        int childCount = parent.getChildCount();
        int limit = childCount - 1;

        for (int i = 0; i < limit; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
