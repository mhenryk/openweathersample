package com.mhmasny.openweathersample.ui.screens.vm;

import android.databinding.ObservableField;

import com.mhmasny.openweathersample.db.model.City;

/**
 * Created by Mateusz on 2/9/2018.
 */

public class DefaultItemViewModel implements ItemViewModel<City> {

    private ObservableField<City> mData = new ObservableField<>();

    @Override
    public ObservableField<City> getData() {
        return mData;
    }

    @Override
    public void setData(City s) {
        mData.set(s);
    }

}
