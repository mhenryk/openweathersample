package com.mhmasny.openweathersample.ui.screens;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mhmasny.openweathersample.R;
import com.mhmasny.openweathersample.databinding.FragmentCityWeatherBinding;
import com.mhmasny.openweathersample.repositories.RepositoryProvider;
import com.mhmasny.openweathersample.ui.screens.vm.CityWeatherViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultCityWeatherViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultMainActivityViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.MainActivityViewModel;

/**
 * Created by Mateusz on 2/9/2018.
 */

public class CityWeatherFragment extends Fragment {

    private FragmentCityWeatherBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_weather, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity activity = getActivity();
        if (activity == null) return;
        final MainActivityViewModel mainActivityViewModel = ViewModelProviders.of(activity)
                                                                              .get(DefaultMainActivityViewModel.class);
        CityWeatherViewModel viewModel = ViewModelProviders.of(this)
                                                           .get(DefaultCityWeatherViewModel.class)
                                                           .weatherRepository(RepositoryProvider.getWeatherRepository());
        viewModel.getCurrentWeatherLiveData().observe(this, currentWeather -> {
            //noop, let the binding do the stuff, we just need the observer to make it working
        });
        viewModel.getLoadingLiveData().observe(this, aBoolean -> {
            //noop
        });
        viewModel.setCity(mainActivityViewModel.selectedCity());
        mBinding.setVm(viewModel);
    }

}
