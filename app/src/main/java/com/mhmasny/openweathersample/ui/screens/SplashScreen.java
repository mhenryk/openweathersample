package com.mhmasny.openweathersample.ui.screens;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mhmasny.openweathersample.R;
import com.mhmasny.openweathersample.repositories.LocationRepository;
import com.mhmasny.openweathersample.repositories.RepositoryProvider;

/**
 * Created by Mateusz on 2/11/2018.
 */

public class SplashScreen extends AppCompatActivity {
    private static final String TAG = "SplashScreen";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LocationRepository repository = RepositoryProvider.getLocationRepository();
        final Observer<Boolean> observer = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                Log.d(TAG, "Update " + aBoolean);
                if (aBoolean != null && aBoolean) {
                    repository.isDatabaseReady().removeObserver(this);
                    startMainActivity();
                }
            }
        };
        repository.isDatabaseReady().observe(this, observer);
        repository.loadCitiesDb(this);
    }

    private void startMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
