package com.mhmasny.openweathersample.ui.screens;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mhmasny.openweathersample.R;
import com.mhmasny.openweathersample.databinding.FragmentCityListBinding;
import com.mhmasny.openweathersample.repositories.RepositoryProvider;
import com.mhmasny.openweathersample.threading.ThreadManager;
import com.mhmasny.openweathersample.ui.screens.adapter.CityListAdapter;
import com.mhmasny.openweathersample.ui.screens.vm.CityListViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultCityListViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultMainActivityViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.MainActivityViewModel;

/**
 * Created by Mateusz on 2/8/2018.
 */

public class CityListFragment extends Fragment {

    private FragmentCityListBinding mBinding;
    private CityListAdapter         mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_list, container, false);
        mBinding.cityList.addItemDecoration(new SeparatorDecoration(getContext(), R.drawable.separator));
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final MainActivityViewModel mainActivityViewModel = ViewModelProviders.of(getActivity())
                                                                              .get(DefaultMainActivityViewModel.class);
        mAdapter = new CityListAdapter(ThreadManager.instance(),
                                       mainActivityViewModel::selectCity);

        CityListViewModel viewModel = ViewModelProviders.of(this)
                                                        .get(DefaultCityListViewModel.class)
                                                        .locationRepository(getActivity(),
                                                                            RepositoryProvider.getLocationRepository());
        viewModel.cityList().observe(this, cities -> mAdapter.setCities(cities));
        viewModel.isLoadingLiveData().observe(this, aBoolean -> {
            //noop
        });
        mBinding.setVm(viewModel);
        mBinding.cityList.setAdapter(mAdapter);
    }
}
