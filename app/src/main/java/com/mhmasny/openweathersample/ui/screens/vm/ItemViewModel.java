package com.mhmasny.openweathersample.ui.screens.vm;

import android.databinding.ObservableField;

/**
 * Created by Mateusz on 2/9/2018.
 */

public interface ItemViewModel<T> {
    ObservableField<T> getData();
    void setData(T t);
}
