package com.mhmasny.openweathersample.ui.screens;

/**
 * Created by Mateusz on 2/9/2018.
 */

public interface ItemClickListener<T> {
    void onItemClicked(T data);
}
