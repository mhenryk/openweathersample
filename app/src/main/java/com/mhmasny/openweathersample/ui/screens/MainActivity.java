package com.mhmasny.openweathersample.ui.screens;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mhmasny.openweathersample.R;
import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultMainActivityViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.MainActivityViewModel;

/**
 * Created by Mateusz on 2/8/2018.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivityViewModel viewModel =
                ViewModelProviders.of(this).get(DefaultMainActivityViewModel.class);
        viewModel.selectedCity().observe(this, this::showCityCurrentWeather);

        Fragment listFragment  =new CityListFragment();
        listFragment.setRetainInstance(true);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                                       .replace(R.id.single_container, listFragment)
                                       .commit();
        }
    }

    private void showCityCurrentWeather(City s) {
        Log.d(TAG, "Show city " + s);
        Fragment weatherFragment = new CityWeatherFragment();
        weatherFragment.setRetainInstance(true);
        getSupportFragmentManager().beginTransaction()
                                   .replace(R.id.single_container, weatherFragment)
                                   .addToBackStack(CityWeatherFragment.class.getCanonicalName())
                                   .commit();
    }
}
