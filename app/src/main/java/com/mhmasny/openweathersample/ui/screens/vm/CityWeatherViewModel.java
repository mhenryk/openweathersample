package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.databinding.ObservableField;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.model.js2p.CurrentWeather;

/**
 * Created by Mateusz on 2/9/2018.
 */

public interface CityWeatherViewModel {
    ObservableField<CurrentWeather> currentWeather();
    LiveData<CurrentWeather> getCurrentWeatherLiveData();
    ObservableField<Boolean> isLoading();
    LiveData<Boolean> getLoadingLiveData();
    void setCity(LiveData<City> stringLiveData);
}
