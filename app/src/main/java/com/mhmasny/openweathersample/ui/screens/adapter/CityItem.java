package com.mhmasny.openweathersample.ui.screens.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mhmasny.openweathersample.databinding.ItemCityBinding;
import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.ui.screens.ItemClickListener;
import com.mhmasny.openweathersample.ui.screens.vm.DefaultItemViewModel;
import com.mhmasny.openweathersample.ui.screens.vm.ItemViewModel;

/**
 * Created by Mateusz on 2/9/2018.
 */

class CityItem extends RecyclerView.ViewHolder {
    private final ItemCityBinding mBinding;

    public CityItem(ItemCityBinding binding, ItemClickListener<City> clickListener) {
        super(binding.getRoot());
        mBinding = binding;
        mBinding.setClickListener(clickListener);
    }

    public static ItemCityBinding newBinding(ViewGroup parent) {
        return ItemCityBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
    }

    public void bind(City city) {
        ItemViewModel<City> vm = new DefaultItemViewModel();
        vm.setData(city);
        mBinding.setVm(vm);
        mBinding.executePendingBindings();
    }
}
