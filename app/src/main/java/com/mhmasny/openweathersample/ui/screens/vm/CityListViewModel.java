package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.repositories.LocationRepository;

import java.util.List;

/**
 * Created by Mateusz on 2/8/2018.
 */

public interface CityListViewModel {
    @NonNull
    LiveData<List<City>> cityList();

    void onTextChanged(CharSequence var1, int var2, int var3, int var4);

    void onCityEntered();

    @NonNull
    ObservableBoolean isLoading();

    @NonNull
    LiveData<Boolean> isLoadingLiveData();

    @NonNull
    CityListViewModel locationRepository(@NonNull Context context, @NonNull LocationRepository repository);
}
