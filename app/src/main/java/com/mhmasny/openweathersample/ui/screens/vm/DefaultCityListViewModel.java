package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.repositories.LocationRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mateusz on 2/8/2018.
 */

public class DefaultCityListViewModel extends ViewModel implements CityListViewModel {

    private static final int MIN_QUERY_LENGTH = 3;

    private LocationRepository mLocationRepository;
    private MediatorLiveData<List<City>> mCityList        = new MediatorLiveData<>();
    private MediatorLiveData<Boolean>    mDatabaseLoading = new MediatorLiveData<>();
    private ObservableBoolean            mIsLoading       = new ObservableBoolean(false);
    private MutableLiveData<String>      mQuery           = new MutableLiveData<>();
    private boolean mWaitingQuery;
    private List<City> mEmptyList = new ArrayList<>();

    @NonNull
    @Override
    public LiveData<List<City>> cityList() {
        return mCityList;
    }

    @Override
    public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
        if (var1.length() == 0) {
            mCityList.setValue(mEmptyList);
            return;
        }
        if (var1.length() < MIN_QUERY_LENGTH) return; //dont query too small names
        mQuery.setValue(String.valueOf(var1));
        if (!mIsLoading.get()) {
            query();
        } else {
            mWaitingQuery = true;
        }
    }

    private void query() {
        if (mQuery.getValue() == null || mQuery.getValue().isEmpty()) return;
        mLocationRepository.queryCities(mQuery.getValue());
    }

    @Override
    public void onCityEntered() {
        query();
    }

    @NonNull
    @Override
    public ObservableBoolean isLoading() {
        return mIsLoading;
    }

    @NonNull
    @Override
    public LiveData<Boolean> isLoadingLiveData() {
        return mDatabaseLoading;
    }

    @NonNull
    @Override
    public CityListViewModel locationRepository(@NonNull Context context, @NonNull LocationRepository repository) {
        mLocationRepository = repository;
        mDatabaseLoading.removeSource(mLocationRepository.isDatabaseLoading()); //android architecture components bug
        mDatabaseLoading.addSource(mLocationRepository.isDatabaseLoading(), aBoolean -> {
            if (aBoolean != null) {
                mDatabaseLoading.setValue(aBoolean);
                mIsLoading.set(aBoolean);
            }
        });

        mCityList.removeSource(mLocationRepository.getCityList());
        mCityList.addSource(mLocationRepository.getCityList(), cities -> {
            mCityList.setValue(cities);
            if (mWaitingQuery) {
                mWaitingQuery = false;
                query();
            }
        });
        return this;
    }
}
