package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.support.annotation.Nullable;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.model.js2p.CurrentWeather;
import com.mhmasny.openweathersample.repositories.CityWeatherRepository;

/**
 * Created by Mateusz on 2/9/2018.
 */

public class DefaultCityWeatherViewModel extends ViewModel implements CityWeatherViewModel {

    @Nullable
    private CityWeatherRepository mWeatherRepository;

    private MediatorLiveData<CurrentWeather> mCurrentWeatherData = new MediatorLiveData<>();
    private ObservableField<CurrentWeather>  mCurrentWeather     = new ObservableField<>();
    private ObservableField<Boolean>         mIsLoading          = new ObservableField<>();

    public DefaultCityWeatherViewModel() {
    }

    public CityWeatherViewModel weatherRepository(CityWeatherRepository weatherRepository) {
        this.mWeatherRepository = weatherRepository;
        mCurrentWeatherData.removeSource(mWeatherRepository.currentWeather());
        mCurrentWeatherData.addSource(mWeatherRepository.isLoading(),this::innerSetLoading);
        mCurrentWeatherData.addSource(mWeatherRepository.currentWeather(),
                                      this::innerSetWeather);
        return this;
    }

    private void innerSetLoading(Boolean loading) {
        if(loading==null) return;
        mIsLoading.set(loading);
        mIsLoading.notifyChange();
    }

    @Override
    public ObservableField<CurrentWeather> currentWeather() {
        return mCurrentWeather;
    }

    @Override
    public void setCity(LiveData<City> cityData) {
        innerSetCity(cityData.getValue());
        mCurrentWeatherData.removeSource(cityData);
        mCurrentWeatherData.addSource(cityData, this::innerSetCity);
    }

    private void innerSetCity(City city) {
        if (city != null) {
            innerSetWeather(city.getCurrentWeather());
            fetch(city);
        }
    }

    private void innerSetWeather(CurrentWeather currentWeather) {
        mCurrentWeatherData.setValue(currentWeather);
        mCurrentWeather.set(currentWeather);
        mCurrentWeather.notifyChange();
    }

    public LiveData<CurrentWeather> getCurrentWeatherLiveData() {
        return mCurrentWeatherData;
    }

    @Override
    public ObservableField<Boolean> isLoading() {
        return mIsLoading;
    }

    @Override
    public LiveData<Boolean> getLoadingLiveData() {
        if(mWeatherRepository==null) return null;
        return mWeatherRepository.isLoading();
    }

    private void fetch(City city) {
        if (mWeatherRepository == null) return;
        mWeatherRepository.getWeatherFor(city);
    }
}
