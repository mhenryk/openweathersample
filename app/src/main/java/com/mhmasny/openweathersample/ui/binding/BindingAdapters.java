package com.mhmasny.openweathersample.ui.binding;

import android.databinding.BindingAdapter;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.transition.TransitionManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mhmasny.openweathersample.R;
import com.mhmasny.openweathersample.model.js2p.Weather;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Mateusz on 2/8/2018.
 */

public class BindingAdapters {
    private static final String TAG = "BindingAdapters";

    @BindingAdapter("onKeyDone")
    public static void onKeyDone(EditText editText, final Runnable action) {
        if (editText != null) {
            editText.setOnKeyListener((v, keyCode, event) -> {
                boolean handled = false;
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && event.getAction() == KeyEvent.ACTION_DOWN) {
                    action.run();
                    handled = true;
                }
                return handled;
            });
        }
    }

    @BindingAdapter("rotation")
    public static void rotation(ImageView imageView, double rotation) {
        RotateAnimation anim = new RotateAnimation(imageView.getRotation(),
                                                   (float) rotation,
                                                   imageView.getX() / 2,
                                                   imageView.getY() / 2);
        anim.setInterpolator(new BounceInterpolator());
        anim.setDuration(300);
        anim.setStartOffset(500);
        anim.start();
    }

    @BindingAdapter("date")
    public static void date(TextView textView, long timestamp) {
        textView.setText(DateFormat.getDateTimeInstance().format(new Date(timestamp)));

    }

    @BindingAdapter("time")
    public static void time(TextView textView, long timestamp) {
        textView.setText(DateFormat.getTimeInstance().format(new Date(timestamp)));
    }

    @BindingAdapter("weatherMain")
    public static void weatherMain(TextView textView, List<Weather> weather) {
        if (weather == null || weather.size() == 0) return;
        String msg = weather.get(0).getMain();
        if (msg == null) return;
        textView.setText(msg);
    }

    @BindingAdapter("weatherSub")
    public static void weatherSub(TextView textView, List<Weather> weather) {
        if (weather == null || weather.size() == 0) return;
        String msg = weather.get(0).getDescription();
        if (msg == null) return;
        textView.setText(msg);
    }

    @BindingAdapter("visibleGone")
    public static void visibleGone(View view, boolean gone) {
        if (view.getParent() instanceof ConstraintLayout) {
            visibleConstraintGone(view, (ConstraintLayout) view.getParent(), gone);
        } else {
            view.setVisibility(gone ? View.GONE : View.VISIBLE);
        }
    }

    private static void visibleConstraintGone(View view, ConstraintLayout layout, boolean gone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(layout);
            ConstraintSet set = new ConstraintSet();
            set.clone(layout);
            set.setVisibility(view.getId(),gone? ConstraintSet.GONE : ConstraintSet.VISIBLE);
            set.applyTo(layout);
        }
    }


    @BindingAdapter("weatherIcon")
    public static void weatherIcon(ImageView imageView, List<Weather> weather) {
        if (weather == null || weather.size() == 0) return;
        String iconId = weather.get(0).getIcon();
        if (iconId == null) return;
        String url = imageView.getContext().getString(R.string.icon_url, iconId);
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    @BindingAdapter("humidity")
    public static void humidity(TextView textView, String value) {
        setText(textView,R.string.humidity,value);
    }
    @BindingAdapter("temperature")
    public static void temperature(TextView textView, String value) {
        setText(textView,R.string.temperature,value);
    }

    private static void setText(TextView textView, @StringRes int res, String arg) {
        textView.setText(textView.getResources().getString(res,arg));
    }
    @BindingAdapter("windSpeed")
    public static void windSpeed(TextView textView, String value) {
        setText(textView,R.string.wind_speed,value);
    }
}
