package com.mhmasny.openweathersample.ui.screens.vm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;

/**
 * Created by Mateusz on 2/8/2018.
 */

public class DefaultMainActivityViewModel extends ViewModel implements MainActivityViewModel {
    private static final String TAG = "DefaultMainActivityView";

    private MutableLiveData<City> mSelectedCity = new MutableLiveData<>();

    @Override
    public void selectCity(@NonNull City city) {
        mSelectedCity.setValue(city);
    }

    @Override
    public LiveData<City> selectedCity() {
        return mSelectedCity;
    }
}
