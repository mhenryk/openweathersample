package com.mhmasny.openweathersample.ui.screens.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.threading.ThreadManager;
import com.mhmasny.openweathersample.ui.screens.ItemClickListener;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by Mateusz on 2/9/2018.
 */

public class CityListAdapter extends RecyclerView.Adapter<CityItem> {

    private final ThreadManager mThreadManager;
    private List<City> mCityList = new ArrayList<>();
    private ItemClickListener<City> mClickListener;
    private Queue<List<City>> pendingUpdates = new ArrayDeque<>();

    public CityListAdapter(ThreadManager threadManager, ItemClickListener<City> mClickListener) {
        this.mClickListener = mClickListener;
        this.mThreadManager = threadManager;
    }

    @Override
    public CityItem onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CityItem(CityItem.newBinding(parent), mClickListener);
    }

    @Override
    public void onBindViewHolder(CityItem holder, int position) {
        holder.bind(get(position));
    }

    @Override
    public int getItemCount() {
        return mCityList.size();
    }

    public City get(int pos) {
        return mCityList.get(pos);
    }

    private void setCitiesInternally(List<City> cities) {
        List<City> current = new ArrayList<>(mCityList);
        List<City> copy = new ArrayList<>(cities);
        mThreadManager.post(()-> {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(newCallback(current, copy), true);
            mThreadManager.postOnUi(()->applyDiffResult(copy,result));
        });
    }

    private DiffUtil.Callback newCallback(List<City> current, List<City> newList) {
        return new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return current.size();
            }

            @Override
            public int getNewListSize() {
                return newList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                return current.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
            }

            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return current.get(oldItemPosition).equals(newList.get(newItemPosition));
            }
        };
    }

    public void setCities(final List<City> newItems) {
        pendingUpdates.add(newItems);
        if (pendingUpdates.size() > 1) {
            return;
        }
        setCitiesInternally(newItems);
    }

    private void applyDiffResult(List<City> newItems,
                                   DiffUtil.DiffResult diffResult) {
        pendingUpdates.remove();
        dispatchUpdates(newItems, diffResult);
        if (pendingUpdates.size() > 0) {
            setCitiesInternally(pendingUpdates.peek());
        }
    }

    private void dispatchUpdates(List<City> newItems,
                                 DiffUtil.DiffResult diffResult) {
        mCityList.clear();
        mCityList.addAll(newItems);
        diffResult.dispatchUpdatesTo(this);
    }
}
