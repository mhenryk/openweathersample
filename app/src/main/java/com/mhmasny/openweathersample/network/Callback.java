package com.mhmasny.openweathersample.network;

/**
 * Created by Mateusz on 2/10/2018.
 */

public interface Callback<T> {
    void onSuccess(T result);
    void onFailure(Throwable t);
}
