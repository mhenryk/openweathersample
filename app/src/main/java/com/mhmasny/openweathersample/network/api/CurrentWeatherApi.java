package com.mhmasny.openweathersample.network.api;

import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.model.js2p.CurrentWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mateusz on 2/10/2018.
 */

public interface CurrentWeatherApi {
    //example url //http://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22

    interface Keys {
        String PATH_CURRENT_WEATHER = "weather";
        String ARG_APP_ID           = "appId";
        String ARG_CITY_ID          = "id";
        String API_KEY              = "bfa8dd0749edc9ca9f8e3ba9870314e5";
        String ARG_UNITS            = "units";
        String UNITS_METRIC         = "metric";
    }

    @GET(Keys.PATH_CURRENT_WEATHER)
    Call<CurrentWeather> getCurrentWeather(@Query(Keys.ARG_APP_ID) @NonNull String appId,
                                           @Query(Keys.ARG_CITY_ID) long id,
                                           @Query(Keys.ARG_UNITS) String units);
}
