package com.mhmasny.openweathersample.network.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.mhmasny.openweathersample.threading.ThreadManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mateusz on 2/10/2018.
 */

public final class ApiProvider {

    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private final OkHttpClient mHttpClient;
    private final Gson         mGson;

    private CurrentWeatherApi mCurrentWeatherApi;

    private ApiProvider() {
        mHttpClient = buildOkHttp();
        mGson = buildGson();
        ExecutorService executor = ThreadManager.instance().executor();
        mCurrentWeatherApi = buildApi(mHttpClient, executor, mGson, BASE_URL, CurrentWeatherApi.class);
    }

    private static class Holder {
        private static final ApiProvider INSTANCE = new ApiProvider();
    }

    private Gson buildGson() {
        return new Gson();
    }

    public static ApiProvider instance() {
        return Holder.INSTANCE;
    }

    @NonNull
    private static <T> T buildApi(OkHttpClient httpClient,
                                  @NonNull ExecutorService executorService,
                                  @NonNull Gson gson,
                                  @NonNull String baseUrl,
                                  @NonNull Class<T> serverClass) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .callbackExecutor(executorService)
                .build()
                .create(serverClass);
    }

    private static OkHttpClient buildOkHttp() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging)
               .retryOnConnectionFailure(true)
               .connectTimeout(15L, TimeUnit.SECONDS);
        return builder.build();
    }

    public Gson gson() {
        return mGson;
    }

    public OkHttpClient okHttp() {
        return mHttpClient;
    }

    public CurrentWeatherApi weatherApi() {
        return mCurrentWeatherApi;
    }
}
