package com.mhmasny.openweathersample.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.mhmasny.openweathersample.db.AppDatabase;
import com.mhmasny.openweathersample.db.AppDatabaseHolder;
import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.model.js2p.CurrentWeather;
import com.mhmasny.openweathersample.network.api.ApiProvider;
import com.mhmasny.openweathersample.network.api.CurrentWeatherApi;
import com.mhmasny.openweathersample.threading.ThreadManager;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mateusz on 2/10/2018.
 */

class DefaultCityWeatherRepository implements CityWeatherRepository {

    private static final long OLD_TIME_DAYS = 1;
    private static final String TAG = "DefaultCityWeatherRepos";
    private CurrentWeatherApi mWeatherApi;
    private MutableLiveData<Boolean>        mIsLoading      = new MutableLiveData<>();
    private MutableLiveData<CurrentWeather> mCurrentWeather = new MutableLiveData<>();
    private MutableLiveData<String>         mError          = new MutableLiveData<>();
    private AppDatabase   mDatabase;
    private ThreadManager mThreadManager;

    private DefaultCityWeatherRepository() {
        initDependencies();
    }

    private static final class Holder {
        private static final DefaultCityWeatherRepository INSTANCE = new DefaultCityWeatherRepository();
    }

    //replacable by DI
    private void initDependencies() {
        mWeatherApi = ApiProvider.instance().weatherApi();
        mDatabase = AppDatabaseHolder.getDatabase();
        mThreadManager = ThreadManager.instance();
    }

    public static CityWeatherRepository instance() {
        return Holder.INSTANCE;
    }

    private void fetch(City city) {
        mIsLoading.postValue(true);
        mWeatherApi.getCurrentWeather(CurrentWeatherApi.Keys.API_KEY, city.getId(), CurrentWeatherApi.Keys.UNITS_METRIC)
                   .enqueue(new Callback<CurrentWeather>() {
                       @Override
                       public void onResponse(@NonNull Call<CurrentWeather> call,
                                              @NonNull Response<CurrentWeather> response) {
                           CurrentWeather body = response.body();
                           if (response.isSuccessful() && body != null) {
                               processSuccess(city, body);
                           }
                       }

                       @Override
                       public void onFailure(@NonNull Call<CurrentWeather> call, @NonNull Throwable t) {
                          t.printStackTrace();
                       }
                   });
    }

    @WorkerThread
    private void processSuccess(@NonNull City city, @NonNull CurrentWeather body) {
        Log.v(TAG, "Response success " + body);
        city.setCurrentWeather(body);
        mDatabase.cityDao().insertAll(city);
        mCurrentWeather.postValue(body);
        mIsLoading.postValue(false);
        mError.postValue(null);
    }

    @Override
    public LiveData<CurrentWeather> currentWeather() {
        return mCurrentWeather;
    }

    @Override
    public void getWeatherFor(@NonNull City city) {
        mError.postValue(null);
        if (mCurrentWeather.getValue() == null
                || !mCurrentWeather.getValue().getName().equals(city.getName())) {
            mCurrentWeather.setValue(null);
            fetch(city);
        } else if (mCurrentWeather.getValue().getName().equals(city.getName()) && isOld(mCurrentWeather.getValue())) {
            readFromDatabaseAndFetch(city);
        }
    }

    private void readFromDatabaseAndFetch(City city) {
        mThreadManager.post(() -> {
            mIsLoading.postValue(true);
            City returnedCity = mDatabase.cityDao().readCity(city.getId());
            if (returnedCity != null && !isOld(returnedCity.getCurrentWeather())) {
                mCurrentWeather.postValue(returnedCity.getCurrentWeather());
            } else {
                fetch(city);
            }
        });
    }

    private boolean isOld(@Nullable CurrentWeather value) {
        if (value == null) return true;
        long time = value.getDt();
        long now = System.currentTimeMillis();
        long diff = now - time;
        if (diff > 0) {
            return TimeUnit.HOURS.toMillis(OLD_TIME_DAYS) < diff;
        } else {
            return true; //data malformed or system clock has bad date, just fetch
        }
    }

    @Override
    public LiveData<Boolean> isLoading() {
        return mIsLoading;
    }

}
