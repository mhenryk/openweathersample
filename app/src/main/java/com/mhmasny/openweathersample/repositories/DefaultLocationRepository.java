package com.mhmasny.openweathersample.repositories;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.mhmasny.openweathersample.db.AppDatabase;
import com.mhmasny.openweathersample.db.AppDatabaseHolder;
import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.threading.ThreadManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Mateusz on 2/9/2018.
 */

class DefaultLocationRepository implements LocationRepository {

    private static final String TAG            = "DefaultLocationReposito";
    private static final String CITY_LIST_FILE = "city_list.json";
    private static final int    BATCH_SIZE     = 5000;

    private MutableLiveData<List<City>> mCityList;
    private AppDatabase                 mDatabase;
    private MutableLiveData<Boolean>    mDatabaseLoading;
    private ThreadManager               mThreadManager;
    private Queue<String>                         mPendingQueries = new ArrayDeque<>();
    private ConcurrentHashMap<String, List<City>> mResultsMap     = new ConcurrentHashMap<>();
    private MutableLiveData<Boolean>              mDatabaseReady  = new MutableLiveData<>();

    private DefaultLocationRepository() {
        mCityList = new MutableLiveData<>();
        mDatabaseLoading = new MutableLiveData<>();
        mCityList.setValue(new ArrayList<>());
        mDatabaseReady.setValue(false);
        initDependencies();
    }

    private static final class Holder {
        private static DefaultLocationRepository INSTANCE = new DefaultLocationRepository();
    }

    public static LocationRepository instance() {
        return Holder.INSTANCE;
    }

    private void initDependencies() {
        mDatabase = AppDatabaseHolder.getDatabase();
        mThreadManager = ThreadManager.instance();
    }

    @Override
    public LiveData<List<City>> getCityList() {
        return mCityList;
    }

    @Override
    public LiveData<Boolean> isDatabaseLoading() {
        return mDatabaseLoading;
    }

    @Override
    public LiveData<Boolean> isDatabaseReady() {
        return mDatabaseReady;
    }

    @Override
    public void loadCitiesDb(@NonNull Context context) {
        mDatabaseReady.setValue(false);
        mThreadManager.post(() -> {
            int count = mDatabase.cityDao().getNumberOfRows();
            if (count == 0) {
                readCityAssets(context);
            }
            mDatabaseReady.postValue(true);
        });
    }

    private void readCityAssets(@NonNull Context context) {
        try {
            InputStream stream = context.getAssets().open(CITY_LIST_FILE);
            JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
            Gson gson = new GsonBuilder().create();
            long start = System.currentTimeMillis();
            Log.d(TAG, "Starting read " + start);
            reader.beginArray();
            List<City> batch = new ArrayList<>(BATCH_SIZE);
            while (reader.hasNext()) {
                City city = gson.fromJson(reader, City.class);
                if (city.getName() != null && !city.getName().isEmpty()) {
                    batch.add(city);
                }
                if (batch.size() == BATCH_SIZE) {
                    mDatabase.cityDao().insertAll(batch.toArray(new City[batch.size()]));
                    batch.clear();
                }
            }
            if (batch.size() > 0) {
                mDatabase.cityDao().insertAll(batch.toArray(new City[batch.size()]));
            }
            long diff = System.currentTimeMillis() - start;
            Log.d(TAG, "Ending read took " + diff + "ms");
            reader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void queryCities(@NonNull String query) {
        mPendingQueries.add(query);
        internalQuery(query);
    }

    private void internalQuery(@NonNull String query) {
        mDatabaseLoading.setValue(mPendingQueries.size() > 0);
        mThreadManager.post(() -> {
            String sqlQuery = "%" + query + "%";
            List<City> cities = mDatabase.cityDao().queryByName(sqlQuery);
            Log.i(TAG, "Printing cities " + (Arrays.toString(cities.toArray())));
            reportResults(query, cities);
        });
    }

    private void reportResults(@NonNull String query, @NonNull List<City> cities) {
        if (mPendingQueries.peek().equals(query)) {
            mCityList.postValue(cities);
            mPendingQueries.remove();
            String next = mPendingQueries.peek();
            if (next != null && mResultsMap.containsKey(next)) {
                List<City> nextCities = mResultsMap.get(next);
                mResultsMap.remove(next);
                reportResults(next, nextCities);
            }
        } else {
            mResultsMap.put(query, cities);
        }
        mDatabaseLoading.postValue(mPendingQueries.size() != 0);
    }
}
