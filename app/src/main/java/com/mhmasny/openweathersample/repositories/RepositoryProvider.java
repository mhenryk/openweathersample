package com.mhmasny.openweathersample.repositories;

/**
 * Created by Mateusz on 2/10/2018.
 */

public final class RepositoryProvider {

    public static LocationRepository getLocationRepository() {
        return DefaultLocationRepository.instance();
    }

    public static CityWeatherRepository getWeatherRepository() {
        return DefaultCityWeatherRepository.instance();
    }


}
