package com.mhmasny.openweathersample.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;
import com.mhmasny.openweathersample.model.js2p.CurrentWeather;

/**
 * Created by Mateusz on 2/10/2018.
 */

public interface CityWeatherRepository {
    LiveData<CurrentWeather> currentWeather();
    void getWeatherFor(@NonNull City city);
    LiveData<Boolean> isLoading();
}
