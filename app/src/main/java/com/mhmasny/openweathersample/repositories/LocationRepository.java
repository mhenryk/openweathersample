package com.mhmasny.openweathersample.repositories;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;

import com.mhmasny.openweathersample.db.model.City;

import java.util.List;

/**
 * Created by Mateusz on 2/9/2018.
 */

public interface LocationRepository {
    LiveData<List<City>> getCityList();
    LiveData<Boolean> isDatabaseLoading();
    LiveData<Boolean> isDatabaseReady();

    void loadCitiesDb(@NonNull Context context);

    void queryCities(@NonNull String query);
}
