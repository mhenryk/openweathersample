package com.mhmasny.openweathersample.threading;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Mateusz on 2/11/2018.
 */

public final class ThreadManager {
    private final ExecutorService mExecutor          = Executors.newCachedThreadPool();
    private final Handler         mMainThreadHandler = new Handler(Looper.getMainLooper());

    private ThreadManager() {
    }

    private static final class Holder {
        private static final ThreadManager INSTANCE = new ThreadManager();
    }

    public ExecutorService executor() {
        return mExecutor;
    }

    public Handler mainHandler() {
        return mMainThreadHandler;
    }

    public static ThreadManager instance() {
        return ThreadManager.Holder.INSTANCE;
    }

    public void post(@NonNull Runnable runnable) {
        mExecutor.submit(runnable);
    }

    public void postOnUi(@NonNull Runnable runnable) {
        mMainThreadHandler.post(runnable);
    }
}
